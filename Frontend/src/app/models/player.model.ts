export class Player {
  constructor(
    public name: string,
    public first_name: string,
    public second_name: string,
    public birthday: string,
    public birth_place: string,
    public weight: number,
    public height: number,
    public position: string,
    public position_short: string,
    public last_team: string,
    public image: string,
  ) {}
}
